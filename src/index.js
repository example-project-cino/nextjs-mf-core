import { getCrews } from './api/crew.js';
import { SumNumber } from './utils/calculate.js';
import variable from './styles/variable.module.scss';
// const  { getCrews } = require('./pages/api/crew')
// const { SumNumber } = require('./utils/calculate')
// const variable = require('./styles/variable.module.scss')

module.exports = {
  getCrews,
  SumNumber,
  variable
}