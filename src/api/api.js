// import { environment } from './environment.js';

// let API_URL = environment.apiUrl;
const environment = {
  apiUrl: 'https://api.spacexdata.com/v4'
};
// export type APIResponse<Custom = { [key: string]: any }> = ({
//   success: true;
//   data: any;
// } & Custom) | ErrorResponse;

// type Params = {
//   headers?: RequestInit['headers'];
//   body?: Record<string, any>;
// };

const processFetch = (res) => (
  Promise.all([res.ok, res.json()])
    .then(([ok, response]) => {
      if (!ok) {
        return Promise.reject(response);
      }
      if (typeof response.success !== 'boolean') return Promise.resolve(({ success: true, data: response }));
      return Promise.resolve(response);
    })
);
const processCatch = (e) => {
  if (typeof window === 'undefined') {
    // console.error(e);
  }
  const errObj = {
    success: false,
    error_code: e?.error_code ?? e?.status ?? 500,
    error_message: e?.error_message ?? e?.message ?? "error",
  };
  return errObj;
};

export const api = {
  get: (url, params) => {
    const headers = params;

    return fetch(`${environment.apiUrl}${url}`, {
      method: 'GET',
      headers: { 
        'Content-Type': 'application/json',
        ...headers 
      }
    }).then((res) => processFetch(res)).catch((e) => processCatch(e));
  },

  post: (url, params) => {
    const { headers = {}, body = {} } = params;

    return fetch(`${environment.apiUrl}${url}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        ...headers,
      },
      body: JSON.stringify(body),
    }).then((res) => processFetch<T>(res)).catch((e) => processCatch(e));
  },
}

// export default api;