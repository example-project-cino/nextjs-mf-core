import { getCrews } from '../api/crew.js';
import { SumNumber } from '../utils/calculate.js';

function App() {
  return null
}

const nextConfig = {
  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json", ".scss", ".css"],
  },
  reactStrictMode: true,
  
  getCrews,
  SumNumber,
}

export default nextConfig
