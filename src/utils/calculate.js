

export function SumNumber(prop) {
  const { num1, num2 } = prop
  return num1 + num2
}