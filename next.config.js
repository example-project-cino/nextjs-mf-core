/** @type {import('next').NextConfig} */
import path from 'path';
// import variable from './src/styles/variable.module.scss'
// require("dotenv").config()
// const { getCrews } = require('./src/api/crew')
// const { SumNumber } = require('./src/utils/calculate')
// const variable = require('./src/styles/variable.module.scss')

const nextConfig = {
  // sassOptions: {
  //   includePaths: [path.join(__dirname, 'styles')],
  // },
  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json", ".scss", ".css", ".html"],
  },
  reactStrictMode: true,
  // getCrews,
  // SumNumber,
  // variable,
}

// module.exports = nextConfig

// module.exports = {
//   reactStrictMode: true,
// };

export default nextConfig
